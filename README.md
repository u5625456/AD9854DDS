# ENGN4200 Individual Project 2018 S1&S2


## Weekly updates

| Week                                  | Contents                                                                  |
| :---:                                 | ---                                                                       |
| Week 6 , 26 Aug - 01 Sep, 2018        | Build repository                                                          |
| Break  , 02 Sep - 08 Sep, 2018        | Finish file sorting for the the transmitter part and upload progress      |                                                 
| Break  , 09 Sep - 15 Sep, 2018        | Aquire Antenna for transeiver                                             |
| Week 7 , 16 Sep - 22 Sep, 2018        | Draft thesis Part 1-4                                                  |
| Week 8 , 23 Sep - 29 Sep, 2018        | Receiver testing                                                                          |
| Week 9 , 30 Sep - 06 Sep, 2018        | Draft thesis                                                                          |
| Week 10, 07 Oct - 13 Oct, 2018        | Complete thesis poster                                                    |
| Week 11, 14 Oct - 20 Oct, 2018        | Receiver final testings, aquire amplifier, poster presentation                     |
| Week 12, 21 Oct - 27 Oct, 2018        | Complete thesis                                                                          |

## Contact

| Name                  | Email                     |
| :---:                 | ---                       |
| Zhenzhen Liu          | u5625456@anu.edu.au       |

## Table Of Contents
1. [Welcome](#1-welcome)
2. [Project Description](#2-project-description)
3. [Project Documents](#2-project-documents)
4. [AD9854 DDS](#3-Transmitter)
5. [RTL-SDR](#4-Receiver)




## 1. Welcome
Welcome to the AD9854 DDS based transceiver of low VHF project. This project is conducting under the ANU Engineering Program ENGN4200 Individual Project.


## 2. Project Description
The aim of this project is to design a low-cost radio transceiver of 70MHz, using the AD9854 Direct Digital Synthesiser and Raspberry Pi. The programming of synced signal will be done in C language. The transmission of samples is uniform in time.
The I and Q synthesizer function, or the RF signal, used for this research topic is: 

I(t)cosωt+Q(t)sinωt

For signal receiver, the version 3 RTL-SDR radio receiver dongle will be used, which samples a radio frequency signal from 50 MHz to 1700MHz and outputs interleaved 8-bit IQ samples at s symbol rate up to 2.4Msps.

## 3. List of project documents 
1. [Individual Project Contract](Project Documents/U5625456_Zhenzhen_Liu_Contract.pdf)
2. [Context Document](Project Documents/U5625456_ZHENZHEN_LIU_ENGN4200_Context_Document.pdf)
3. [Midterm Report](Project Documents/Midterm/U5625456_Zhenzhen_Liu_ENGN4200_Midterm_Review.pdf)
4. [Thesis Poster](Project Documents/Poster/U5625456_Zhenzhen_Liu_Engn4200_thesis_poster.pdf)
5. [Thesis](Thesis/U5625456_Zhenzhen_Liu_Thesis_2018.pdf)


## 4. AD9854 Direct Digital Synthesiser
[Work repository](AD9854)

## 5. RTL-SDR radio receiver 
[Work repository](RTL-SDR)